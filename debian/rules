#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=2

DH_OPTIONS=--with quilt

# ensure the use of all hardening flags
DEB_BUILD_MAINT_OPTIONS=hardening=+all
export DEB_BUILD_MAINT_OPTIONS

# Avoid lots of warnings while compiling:
# # warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE"
DEB_CPPFLAGS_MAINT_APPEND=-D_DEFAULT_SOURCE
export DEB_CPPFLAGS_MAINT_APPEND

# export necessary for (hardening) flags for perl
DEB_CPPFLAGS += `dpkg-buildflags --get CPPFLAGS`
DEB_CFLAGS += `dpkg-buildflags --get CFLAGS`
DEB_LDFLAGS = `dpkg-buildflags --get LDFLAGS`
export DEB_CPPFLAGS DEB_CFLAGS DEB_LDFLAGS

CONFIGURE_OPTIONS=\
	--enable-debian       \
	--enable-debug        \
	--disable-profiling   \
	--enable-owshell      \
	--enable-owlib        \
	--enable-ownetlib     \
	--enable-i2c          \
	--enable-owhttpd      \
	--enable-owftpd       \
	--enable-owserver     \
	--enable-owexternal   \
	--enable-ownet        \
	--enable-owtap        \
	--disable-owmalloc    \
	--disable-owtraffic   \
	--enable-owmon        \
	--enable-owcapi       \
	--enable-swig         \
	--enable-owperl       \
	--enable-owphp        \
	--disable-owpython    \
	--enable-owtcl        \
	--enable-owfs         \
	--enable-zero         \
	--enable-usb          \
	--enable-parport      \
	--enable-avahi        \
	--enable-ftdi

include /usr/share/dpkg/architecture.mk
ifeq ($(DEB_HOST_ARCH_OS),linux)
CONFIGURE_OPTIONS += --enable-w1
else
CONFIGURE_OPTIONS += --disable-w1
endif

%:
	dh $@ $(DH_OPTIONS)

.PHONY: override_dh_autoreconf override_dh_auto_configure override_dh_auto_test override_dh_missing
override_dh_autoreconf:
	test -f README || cp README.md README
	dh_autoreconf

override_dh_auto_configure:
	dh_auto_configure -- $(CONFIGURE_OPTIONS)

override_dh_auto_test:
	# no tests available

override_dh_auto_install:
	dh_auto_install
	# do not install partial manpage with generic names. Instead, include
	# text directly in manpages and remove them.
	cd debian/tmp/usr/share/man && \
	for manfile in $$(find . -iname "*.[135]") ; do \
		for i in $$(sed -n '2,$$ s/^.so //p' $$manfile | tac); do \
			printf "%s\n" H /$$(echo "$$i" | sed -e 's,/,\\/,g')/d - ".r ./$$i" ",w" \
				| ed -s $$manfile  ; \
		done ; \
	done
	find debian/tmp/usr/share/man -iname "*.[135n]so" -exec $(RM) {} \;
	# move manpage to proper section
	for file in owcapi ownet libowcapi libownet ownetapi ownetlib ; do \
		mv debian/tmp/usr/share/man/man1/$$file.1 debian/tmp/usr/share/man/man3/$$file.3 ;\
	done

override_dh_missing:
	dh_missing --fail-missing -X.la -Xperllocal.pod

.PHONY: override_dh_makeshlibs
override_dh_makeshlibs:
	dh_makeshlibs --no-package libow-tcl
	echo "php:Depends=phpapi-$$(php-config --phpapi)" >> debian/libow-php7.substvars

clean:
	$(RM) README
	dh clean $(DH_OPTIONS)
	# remove autoreconf modifications
	find . -name Makefile.am | while read f ; do \
		$(RM) "$$(dirname "$$f")/$$(basename "$$f" .am).in" ; \
	done
	$(RM) INSTALL aclocal.m4 configure
	$(RM) module/owcapi/src/example++/Makefile \
		module/owcapi/src/example/Makefile \
		module/ownet/c/src/example/Makefile \
		module/ownet/php/examples/ownet_example.php \
		module/swig/php/example/load_php_OW.php \
		module/swig/perl5/OW/Makefile.linux \
		module/swig/perl5/OW/Makefile.osx
	# if this is a git repository, restore removed files that would have
	# been ignored by dpkg-source
	-test -d .git && git checkout -- $$(git status --porcelain | \
		sed -e '/^ D /s/^ D //p;d' | grep -v '^debian/')
	:

override_dh_shlibdeps:
	dh_shlibdeps -a -- --warnings=7
